//
//  HTTPInvoker.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 24/01/22.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum HTTPHeaders {
    case getAllProducts
    case searchProduct
}

private let baseURL: String = "https://api.mercadolibre.com/"

class HTTPInvoker {
    public func getAllProducts(_ strCategory: String) -> URL? {
        return URL(string: "\(baseURL)sites/MLA/search?category=\(strCategory)")
    }
    
    public func searchProduct(_ strProductID: String) -> URL? {
        return URL(string: "\(baseURL)sites/MLA/search?q=\(strProductID)")
    }
}
