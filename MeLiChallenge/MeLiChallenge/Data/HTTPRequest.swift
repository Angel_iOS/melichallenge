//
//  HTTPRequest.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 24/01/22.
//

import Foundation

class HTTPRequest {
    func getData(url: URL, httpMethod: HTTPMethod, completion: @escaping (Result<Data?, Error>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {  data, response, error in
            DispatchQueue.main.async {
                if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    completion(.success(data))
                } else if let error = error {
                    completion(.failure(error))
                }
            }
            
        })
        task.resume()
    }
}
