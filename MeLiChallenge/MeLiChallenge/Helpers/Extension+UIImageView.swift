//
//  Extension+UIImageView.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import UIKit.UIImageView

extension UIImageView {
    func load(url: URL?) {
        guard let url = url else { return }
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
        }
    }
}
