//
//  Extension+Double.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 31/01/22.
//

import Foundation

extension Double {
    var currencyStyle: String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "es_MX")
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.maximumFractionDigits = 2
        
        let ammountFormatted: String = currencyFormatter.string(from: NSNumber(value: self)) ?? String(format: "$%.2f", self)
        return ammountFormatted
    }
}
