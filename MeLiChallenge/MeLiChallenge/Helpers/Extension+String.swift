//
//  Extension+String.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 31/01/22.
//

import Foundation

extension String {
    public var removingWhitespacesAndNewlines: String {
        return components(separatedBy: .whitespacesAndNewlines).joined()
    }
}
