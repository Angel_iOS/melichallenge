//
//  Extension+UIViewController.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import UIKit.UIViewController

extension UIViewController {
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Mercado Libre", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func activityStartAnimating() {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        backgroundView.tag = -1000
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        backgroundView.addSubview(activityIndicator)
        self.view.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = self.view.viewWithTag(-1000){
            background.removeFromSuperview()
        }
        self.view.isUserInteractionEnabled = true
    }
    
}
