//
//  Extension+UITableViewCell.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import UIKit.UITableViewCell

extension UITableViewCell {
    class var identifier: String { return String(describing: self) }
}
