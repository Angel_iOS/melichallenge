//
//  ProductListViewController.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import UIKit

class ProductListViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var txfSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnClean: UIButton!
    
    // MARK: - VARIABLES
    var productListPresenter: ViewToPresenterProductListProtocol?
    private var productsList = [ResultProduct]()
    private var bSearchProduct = false

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProducts()
        setupViewComponents()
    }
    
    // MARK: - LOAD DATA
    private func loadProducts() {
        self.activityStartAnimating()
        txfSearch.text = ""
        productListPresenter?.fetchProducts("MLA1055")
    }
    
    private func searchProduct(_ strProduct: String) {
        self.activityStartAnimating()
        productListPresenter?.searchProducts(strProduct)
    }
    
    // MARK: - SETUP VIEW COMPONENTS
    private func setupViewComponents() {
        self.title = "Mercado Libre"
        self.navigationController?.isNavigationBarHidden = false
        overrideUserInterfaceStyle = .light
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: ProductViewCell.identifier, bundle: nil), forCellReuseIdentifier: ProductViewCell.identifier )
        
        txfSearch.delegate = self
        txfSearch.autocorrectionType = .no
    }
    
    // MARK: - USER INTERACTION
    @IBAction func performSearchProduct(_ sender: UIButton) {
        self.view.endEditing(true)
        if let strProduct = txfSearch.text, !strProduct.isEmpty {
            searchProduct(strProduct.removingWhitespacesAndNewlines)
            bSearchProduct = true
        } else {
            showAlert("Ingrese un producto a buscar")
        }
    }
    
    @IBAction func performCleanSearch(_ sender: UIButton) {
        loadProducts()
        btnClean.isHidden = true
        bSearchProduct = false
    }
}

// MARK: - ARCHITECTURE PROTOCOLS
extension ProductListViewController: PresenterToViewProductListProtocol {
    func showProducts(_ productList: [ResultProduct]) {
        btnClean.isHidden = !bSearchProduct && productList.count > 0
        self.activityStopAnimating()
        self.productsList = productList
        tableView.reloadData()
    }
    
    func showError() {
        self.activityStopAnimating()
        showAlert("Ocurrio un error al cargar los productos.")
    }
}

// MARK: - TEXTFIELDS DELEGATE METHODS
extension ProductListViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

// MARK: - TABLE VIEW DATA SOURCE
extension ProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductViewCell.identifier, for: indexPath) as? ProductViewCell else { return UITableViewCell() }
        cell.setFields(productsList[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - TABLE VIEW DELEGATE METHODS
extension ProductListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        productListPresenter?.showProductDetail(navigationController, product: productsList[indexPath.row])
    }
}
