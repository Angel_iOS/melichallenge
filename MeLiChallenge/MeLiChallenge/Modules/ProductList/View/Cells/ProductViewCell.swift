//
//  ProductViewCell.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import UIKit

class ProductViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    func setFields(_ product: ResultProduct) {
        if let strTitle = product.title, let strThumbnail = product.thumbnail {
            lblProductName.text = strTitle
            imgProduct.load(url: URL(string: strThumbnail))
        }
    }
}
