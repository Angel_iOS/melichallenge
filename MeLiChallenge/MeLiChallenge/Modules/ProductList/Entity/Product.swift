//
//  Product.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import Foundation

struct Product: Codable {
    let results: [ResultProduct]

    enum CodingKeys: String, CodingKey {
        case results
    }
}

struct ResultProduct: Codable {
    let id: String?
    let title: String?
    let price: Double?
    let availableQuantity: Int?
    let soldQuantity: Int?
    let permalink: String?
    let thumbnail: String?
    let acceptsMercadopago: Bool?
    let catalogProductID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case price
        case availableQuantity = "available_quantity"
        case soldQuantity = "sold_quantity"
        case permalink
        case thumbnail
        case acceptsMercadopago = "accepts_mercadopago"
        case catalogProductID = "catalog_product_id"
    }
}
