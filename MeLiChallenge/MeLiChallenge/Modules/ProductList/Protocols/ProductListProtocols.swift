//
//  ProductListProtocols.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import Foundation
import UIKit.UINavigationController

// MARK: - ARCHITECTURE PROTOCOLS
protocol ViewToPresenterProductListProtocol: AnyObject {
    var view: PresenterToViewProductListProtocol? {get set}
    var interactor: PresenterToInteractorProductListProtocol? {get set}
    var router: PresenterToRouterProductListProtocol? {get set}
    func fetchProducts(_ strCategory: String)
    func searchProducts(_ strProduct: String)
    func showProductDetail(_ navigationController: UINavigationController?, product: ResultProduct)
}

protocol PresenterToViewProductListProtocol: AnyObject {
    func showProducts(_ productList: [ResultProduct])
    func showError()
}

protocol PresenterToRouterProductListProtocol: AnyObject {
    static func createProductListScreen() -> ProductListViewController
    func pushToProductDetailModule(_ navigationController: UINavigationController?, product: ResultProduct)
}

protocol PresenterToInteractorProductListProtocol: AnyObject {
    var presenter: InteractorToPresenterProductListProtocol? {get set}
    func fetchProducts(_ strCategory: String)
    func searchProducts(_ strProduct: String)
}

protocol InteractorToPresenterProductListProtocol: AnyObject {
    func productsListFetchedSuccess(_ productList: [ResultProduct])
    func serviceError()
}
