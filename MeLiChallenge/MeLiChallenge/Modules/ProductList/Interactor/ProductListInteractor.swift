//
//  ProductListInteractor.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import Foundation

class ProductListInteractor: PresenterToInteractorProductListProtocol {
    var presenter: InteractorToPresenterProductListProtocol?

    func fetchProducts(_ strCategory: String) {
        let httpRequest = HTTPRequest()
        guard let url = HTTPInvoker().getAllProducts(strCategory) else { return }
        httpRequest.getData(url: url, httpMethod: .get, completion: { [weak self] result in
            switch result {
            case .success(let data):
                if let data = data, let products = try? JSONDecoder().decode(Product.self, from: data) {
                    self?.presenter?.productsListFetchedSuccess(products.results)
                }
            case .failure(_):
                self?.presenter?.serviceError()
            }
        })
    }
    
    func searchProducts(_ strProduct: String) {
        let httpRequest = HTTPRequest()
        guard let url = HTTPInvoker().searchProduct(strProduct) else { return }
        httpRequest.getData(url: url, httpMethod: .get, completion: { [weak self] result in
            switch result {
            case .success(let data):
                if let data = data, let products = try? JSONDecoder().decode(Product.self, from: data) {
                    self?.presenter?.productsListFetchedSuccess(products.results)
                }
            case .failure(_):
                self?.presenter?.serviceError()
            }
        })
    }
}
