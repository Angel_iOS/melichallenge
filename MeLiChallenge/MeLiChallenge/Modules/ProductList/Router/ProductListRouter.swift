//
//  ProductListRouter.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import Foundation
import UIKit.UINavigationController

class ProductListRouter: PresenterToRouterProductListProtocol {
    static func createProductListScreen() -> ProductListViewController {
        let view = ProductListViewController(nibName: "ProductListView", bundle: nil)
        let presenter: ViewToPresenterProductListProtocol & InteractorToPresenterProductListProtocol = ProductListPresenter()
        let interactor: PresenterToInteractorProductListProtocol = ProductListInteractor()
        let router: PresenterToRouterProductListProtocol = ProductListRouter()
        
        view.productListPresenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
    
    func pushToProductDetailModule(_ navigationController: UINavigationController?, product: ResultProduct) {
        let viewController = ProductRouter.createProductScreen(product: product)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
