//
//  ProductListPresenter.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 23/01/22.
//

import Foundation
import UIKit.UINavigationController

class ProductListPresenter: ViewToPresenterProductListProtocol {
    var view: PresenterToViewProductListProtocol?
    var interactor: PresenterToInteractorProductListProtocol?
    var router: PresenterToRouterProductListProtocol?

    func fetchProducts(_ strCategory: String) {
        interactor?.fetchProducts(strCategory)
    }
    
    func searchProducts(_ strProduct: String) {
        interactor?.searchProducts(strProduct)
    }
    
    func showProductDetail(_ navigationController: UINavigationController?, product: ResultProduct) {
        router?.pushToProductDetailModule(navigationController, product: product)
    }
}

// MARK: - ARCHITECTURE EXTENSION
extension ProductListPresenter: InteractorToPresenterProductListProtocol {
    func productsListFetchedSuccess(_ productList: [ResultProduct]) {
        view?.showProducts(productList)
    }
    
    func serviceError() {
        view?.showError()
    }
    
}
