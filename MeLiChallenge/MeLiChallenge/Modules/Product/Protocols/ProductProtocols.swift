//
//  ProductProtocols.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import Foundation
import UIKit.UINavigationController

// MARK: - ARCHITECTURE PROTOCOLS
protocol ViewToPresenterProductProtocol: AnyObject {
    var router: PresenterToRouterProductProtocol? {get set}
}

protocol PresenterToRouterProductProtocol: AnyObject {
    static func createProductScreen(product: ResultProduct) -> ProductViewController
}
