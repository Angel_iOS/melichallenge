//
//  ProductViewController.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import UIKit

class ProductViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblProductLink: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    
    // MARK: - VARIABLES
    var product: ResultProduct?

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
        overrideUserInterfaceStyle = .light
    }
    
    private func setupViewComponents() {
        if let strThumbnail = product?.thumbnail {
            imgProduct.load(url: URL(string: strThumbnail))
        }
        
        lblProductName.text = product?.title
        
        lblProductPrice.text = product?.price?.currencyStyle
        
        lblProductLink.isUserInteractionEnabled = true
        lblProductLink.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        lblProductLink.addGestureRecognizer(tapGesture)
        
        if let bPayment = product?.acceptsMercadopago {
            lblPayment.isHidden = !bPayment
        }
    }
    
    // MARK: - USER INTERACTION
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let strLink = product?.permalink else { return }
        if let url = URL(string: strLink) {
            UIApplication.shared.open(url)
        }
    }
}
