//
//  ProductRouter.swift
//  MeLiChallenge
//
//  Created by José Ángel López on 30/01/22.
//

import Foundation
import UIKit.UINavigationController

class ProductRouter: PresenterToRouterProductProtocol {
    static func createProductScreen(product: ResultProduct) -> ProductViewController {
        let view = ProductViewController(nibName: "ProductView", bundle: nil)
        let presenter: ViewToPresenterProductProtocol = ProductPresenter()
        let router: PresenterToRouterProductProtocol = ProductRouter()
        
        view.product = product
        presenter.router = router
        return view
    }
}
